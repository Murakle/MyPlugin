package SettingsTests;

import Settings.AppSettingsConfigurable;
import com.intellij.testFramework.fixtures.BasePlatformTestCase;

import static Settings.AppSettingsConfigurable.*;

public class PowerFieldTest extends BasePlatformTestCase {

    public void testPowerInRange() {
        AppSettingsConfigurable conf = new AppSettingsConfigurable();
        conf.createComponent();
        AppSettingsComponent settings = conf.getMySettings();
        settings.createUIComponents();
        settings.setPower(0);
        assertEquals((Integer) 0, settings.getPower());
        settings.setPower(50);
        assertEquals((Integer) 50, settings.getPower());
        settings.setPower(100);
        assertEquals((Integer) 100, settings.getPower());
    }

    public void testPowerOutOfRange() {
        AppSettingsConfigurable conf = new AppSettingsConfigurable();
        conf.createComponent();
        AppSettingsComponent settings = conf.getMySettings();
        settings.createUIComponents();
        settings.setPower(-1);
        assertEquals((Integer)0, settings.getPower());
        settings.setPower(101);
        assertEquals((Integer)100, settings.getPower());
    }


}
