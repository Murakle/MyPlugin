package SettingsTests;

import Filter.ImageFilters;
import Settings.AppSettingsConfigurable;
import com.intellij.testFramework.fixtures.BasePlatformTestCase;
import org.junit.Assert;

import static Settings.AppSettingsConfigurable.*;

public class FilterMenuTest extends BasePlatformTestCase {

    public void testFilterChoose() {
        AppSettingsConfigurable conf = new AppSettingsConfigurable();
        conf.createComponent();
        AppSettingsComponent settings = conf.getMySettings();
        settings.createUIComponents();
        settings.setFilter(ImageFilters.NONE);
        assertEquals(ImageFilters.NONE, settings.getFilter());

        settings.setFilter(ImageFilters.BLACK_WHITE);
        assertEquals(ImageFilters.BLACK_WHITE, settings.getFilter());

        settings.setFilter(ImageFilters.LINEAR_BLUR_FILTER);
        assertEquals(ImageFilters.LINEAR_BLUR_FILTER, settings.getFilter());
    }

    public void testNullFilter() {
        AppSettingsConfigurable conf = new AppSettingsConfigurable();
        conf.createComponent();
        AppSettingsComponent settings = conf.getMySettings();
        try {
            settings.setFilter(null);
            Assert.fail("NPE wasn't cached");
        } catch (NullPointerException e) {
            assertEquals("Filter equals null", e.getMessage());
        }
    }
}
