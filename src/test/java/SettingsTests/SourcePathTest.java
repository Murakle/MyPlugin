package SettingsTests;

import Filter.ImageFilters;
import Settings.AppSettingsConfigurable;

import com.intellij.testFramework.fixtures.BasePlatformTestCase;
import org.junit.Assert;

import static Settings.AppSettingsConfigurable.*;

public class SourcePathTest extends BasePlatformTestCase {


    public void testEmptyField() {
        AppSettingsConfigurable conf = new AppSettingsConfigurable();
        conf.createComponent();
        AppSettingsComponent settings = conf.getMySettings();
        settings.createUIComponents();
        settings.setSourcePath(null);
        assertEquals("", settings.getSourcePath());
        settings.setSourcePath("");
        assertEquals("", settings.getSourcePath());
        settings.setSourcePath("foo");
        assertEquals("foo", settings.getSourcePath());
    }




    public void testNotCorrectSourcePath() {
        AppSettingsConfigurable conf = new AppSettingsConfigurable();
        conf.createComponent();
        AppSettingsComponent settings = conf.getMySettings();
        settings.createUIComponents();
        String project = System.getProperty("user.dir");
        String mySourcePath = "not/existing/path";
        String myResultPath = project + "/src/test/resources/result/";
        settings.setSourcePath(mySourcePath);
        settings.setResultPath(myResultPath);
        settings.setFilter(ImageFilters.NONE);
        try {
            conf.apply();
            Assert.fail("IOException wasn't cached");
        } catch (RuntimeException e) {
            assertEquals("abc", "Not correct input path", e.getMessage());
        }
    }
}
