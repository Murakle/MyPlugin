package SettingsTests;

import Filter.ImageFilters;
import Settings.AppSettingsConfigurable;
import com.intellij.testFramework.fixtures.BasePlatformTestCase;
import org.junit.Assert;

import static Settings.AppSettingsConfigurable.*;

public class ResultPathTest extends BasePlatformTestCase {

    public void testResultPathField() {
        AppSettingsConfigurable conf = new AppSettingsConfigurable();
        conf.createComponent();
        AppSettingsComponent settings = conf.getMySettings();
        settings.createUIComponents();
        settings.setResultPath(null);
        assertEquals("", settings.getResultPath());
        settings.setResultPath("");
        assertEquals("", settings.getResultPath());
        settings.setResultPath("foo");
        assertEquals("foo", settings.getResultPath());
    }

    public void testNotCorrectResultPath() {
        AppSettingsConfigurable conf = new AppSettingsConfigurable();
        conf.createComponent();
        AppSettingsComponent settings = conf.getMySettings();
        settings.createUIComponents();
        String project = System.getProperty("user.dir");
        String mySourcePath = project + "/src/test/resources/16.jpg/";
        String myResultPath = "not/existing/path";
        settings.setSourcePath(mySourcePath);
        settings.setResultPath(myResultPath);
        settings.setFilter(ImageFilters.NONE);
        try {
            conf.apply();
            Assert.fail("IOException wasn't cached");
        } catch (RuntimeException e) {
            assertEquals("abc", "Not correct output path: not/existing/path/", e.getMessage());
        }
    }

}
