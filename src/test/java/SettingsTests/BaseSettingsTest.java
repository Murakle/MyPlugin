package SettingsTests;

import Filter.ImageFilters;
import Settings.AppSettingsConfigurable;
import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.wm.impl.IdeBackgroundUtil;
import com.intellij.testFramework.fixtures.BasePlatformTestCase;
import org.junit.Assert;

import static Settings.AppSettingsConfigurable.*;

public class BaseSettingsTest extends BasePlatformTestCase {

    public void testBaseFunc() {
        AppSettingsConfigurable conf = new AppSettingsConfigurable();
        conf.createComponent();
//        assertFalse(conf.isModified());
        assertEquals("Background Image", conf.getDisplayName());
    }

    public void testChangeBGImage() {
        AppSettingsConfigurable conf = new AppSettingsConfigurable();
        conf.createComponent();

        AppSettingsComponent settings = conf.getMySettings();
        settings.createUIComponents();
        String project = System.getProperty("user.dir");
        String mySourcePath = project + "/src/test/resources/16.jpg";
        String myResultPath = project + "/src/test/resources/result/";
        settings.setSourcePath(mySourcePath);
        settings.setResultPath(myResultPath);
        settings.setFilter(ImageFilters.NONE);
        try {
            conf.apply();
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }
        String expectedResultPath = project + "/src/test/resources/result/MyPluginResult1.jpg";
        PropertiesComponent properties = PropertiesComponent.getInstance();
        String actualResultPath;
        try {
            actualResultPath = properties.getValue(IdeBackgroundUtil.EDITOR_PROP).split(",")[0];
        } catch (NullPointerException e) {
            actualResultPath = "";
            Assert.fail("Couldn't get BG path =" + properties.getValue(IdeBackgroundUtil.EDITOR_PROP) + "]");


        }
        assertEquals(expectedResultPath + "; " + actualResultPath, expectedResultPath, actualResultPath);
    }

}
