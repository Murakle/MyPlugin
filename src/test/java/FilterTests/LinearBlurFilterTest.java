package FilterTests;

import Filter.ImageFilters;
import org.junit.Test;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import static org.junit.Assert.assertEquals;

@SuppressWarnings("UseJBColor")
public class LinearBlurFilterTest {

    @SuppressWarnings("UndesirableClassUsage")
    @Test
    public void ApplyLinearBlurFilterTest() throws IOException {

        int side = 3;

        BufferedImage image = new BufferedImage(side, side, BufferedImage.TYPE_INT_RGB);
        int power = 20;
        int amountOfTests = 100;
        Random random = new Random();

        for (int i = 0; i < amountOfTests; i++) {
            BufferedImage expectedResult = new BufferedImage(side, side, BufferedImage.TYPE_INT_RGB);
            for (int j = 0; j < side; j++) {
                for (int k = 0; k < side; k++) {
                    int r = random.nextInt(256);
                    int g = random.nextInt(256);
                    int b = random.nextInt(256);
                    image.setRGB(j, k, new Color(r, g, b).getRGB());
                }
            }
            for (int j = 0; j < side; j++) {
                for (int k = 0; k < side; k++) {
                    int r = 0, g = 0, b = 0;
                    for (int l = 0; l < side; l++) {
                        for (int m = 0; m < side; m++) {
                            Color c = getColor(image, j - (l - side / 2), k - (m - side / 2), side);
                            r += c.getRed();
                            g += c.getGreen();
                            b += c.getBlue();
                        }
                    }
                    r = (int) (r * (float) 1 / 9);
                    g = (int) (g * (float) 1 / 9);
                    b = (int) (b * (float) 1 / 9);
                    expectedResult.setRGB(j, k, new Color(r, g, b).getRGB());
                }
            }
            BufferedImage result = getFilter().getFilter(image, 20).apply().getResult();
            for (int j = 0; j < side; j++) {
                for (int k = 0; k < side; k++) {
                    assertEquals(expectedResult.getRGB(j, k), result.getRGB(j, k));
                }
            }
        }
    }


    @Test(expected = NullPointerException.class)
    public void ApplyFilterNullImageTest() {
        getFilter().getFilter(null, 1).apply();
    }

    private Color getColor(BufferedImage image, int x, int y, int side) {
        int xx = normalize(x, side);
        int yy = normalize(y, side);
        return new Color(image.getRGB(xx, yy));
    }

    private int normalize(int number, int limitation) {
        return Math.min(limitation - 1, Math.max(number, 0));
    }

    public ImageFilters getFilter() {
        return ImageFilters.LINEAR_BLUR_FILTER;
    }
}
