package FilterTests;

import Filter.ImageFilters;
import com.intellij.ui.Gray;
import org.junit.Test;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class BlackWhiteFilterTest {

    @Test
    @SuppressWarnings("UseJBColor")
    public void ApplyBlackWhiteFilterTest() {
        Color color = new Color(100, 150, 200);
        //noinspection UndesirableClassUsage
        BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        image.setRGB(0, 0, color.getRGB());

        Color expectedResult = Gray._150; // (150, 150, 150)
        Color actualResult = new Color(getFilter().getFilter(image, 1).apply().getResult().getRGB(0, 0));

        assertEquals(expectedResult, actualResult);
    }

    @Test(expected = NullPointerException.class)
    public void ApplyFilterNullImageTest() {
        getFilter().getFilter(null, 1).apply();
    }

    public ImageFilters getFilter() {
        return ImageFilters.BLACK_WHITE;
    }
}
