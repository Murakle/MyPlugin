package Settings;

import DrawImageService.DrawImageService;
import Filter.ImageFilters;
import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.options.Configurable;

import com.intellij.openapi.ui.ComboBox;
import com.intellij.openapi.ui.TextComponentAccessor;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.ui.CollectionComboBoxModel;
import com.intellij.ui.ComboboxWithBrowseButton;
import com.intellij.util.containers.ContainerUtil;
import com.intellij.util.ui.UIUtil;
import org.intellij.images.fileTypes.ImageFileTypeManager;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.Nullable;


import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.io.File;
import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

public class AppSettingsConfigurable implements Configurable, Configurable.NoScroll {

    private AppSettingsComponent mySettings;

//    public AppSettingsConfigurable() {
//        mySettings = new AppSettingsComponent();
//    }

    @Override
    public @Nls(capitalization = Nls.Capitalization.Title) String getDisplayName() {
        return "Background Image";
    }

    @Override
    public @Nullable JComponent createComponent() {
        if (mySettings == null) {
            mySettings = new AppSettingsComponent();
        }

        return mySettings.mainPanel;
    }

    public AppSettingsComponent getMySettings() {
        return mySettings;
    }

    @Override
    public boolean isModified() {
        AppSettingsState settings = AppSettingsState.getInstance();
        return !(mySettings.getSourcePath().equals(settings.src) &&
                mySettings.getResultPath().equals(settings.res) &&
                mySettings.getFilter().equals(settings.filter) &&
                mySettings.getPower().equals(settings.power));
    }

    @Override
    public void apply() throws RuntimeException {
        AppSettingsState settings = AppSettingsState.getInstance();
        settings.src = mySettings.getSourcePath();
        String resPath = mySettings.getResultPath();
        settings.res = resPath + (resPath.endsWith("/") ? "" : "/");
        settings.filter = mySettings.getFilter();
        settings.power = mySettings.getPower();
        settings.index = settings.index + 1;
        mySettings.storeRecentImages();

        try {
            DrawImageService.Draw();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public void reset() {
        AppSettingsState settings = AppSettingsState.getInstance();
        mySettings.setSourcePath(settings.src);
        mySettings.setResultPath(settings.res);
        mySettings.setFilter(settings.filter);
        mySettings.setPower(settings.power);
    }

    @Override
    public void disposeUIResources() {
        mySettings = null;
    }

    public static class AppSettingsComponent {
        private JPanel mainPanel;
        private JSlider PowerSlider;
        private ComboboxWithBrowseButton sourcePath; //.jpg

        private ComboboxWithBrowseButton resultPath; // dir
        private JComboBox<String> filters;

        AppSettingsComponent() {

        }

        public String getSourcePath() {
            return (String) sourcePath.getComboBox().getEditor().getItem();
        }

        public void setSourcePath(String text) {
            sourcePath.getComboBox().getEditor().setItem(text);
            setSelectedPath(text, sourcePath);

        }

        public String getResultPath() {
            return (String) resultPath.getComboBox().getEditor().getItem();
        }

        public void setResultPath(String text) {
            resultPath.getComboBox().getEditor().setItem(text);
            setSelectedPath(text, resultPath);
        }

        public ImageFilters getFilter() {
            return ImageFilters.values()[filters.getSelectedIndex()];
        }

        public Integer getPower() {
            return PowerSlider.getValue();
        }

        public void setPower(int value) {
            PowerSlider.setValue(value);
        }

        public void setFilter(ImageFilters filter) {
            if (filter == null) {
                throw new NullPointerException("Filter equals null");
            }
            filters.setSelectedIndex(filter.getIndex());
        }

        private CollectionComboBoxModel<String> getComboModel(ComboboxWithBrowseButton field) {
            //noinspection unchecked
            return (CollectionComboBoxModel<String>) field.getComboBox().getModel();
        }

        private JTextComponent getComboEditor(ComboboxWithBrowseButton field) {
            return (JTextComponent) field.getComboBox().getEditor().getEditorComponent();
        }

        public void setSelectedPath(String path, ComboboxWithBrowseButton field) {
            if (StringUtil.isEmptyOrSpaces(path)) {
                getComboEditor(field).setText("");
            } else {
                CollectionComboBoxModel<String> comboModel = getComboModel(field);
                if (!comboModel.contains(path)) {
                    comboModel.add(path);
                }
                comboModel.setSelectedItem(path);
                getComboEditor(field).setCaretPosition(0);
            }
        }


        public void createUIComponents() {
            filters = new ComboBox<>();
            filters.setModel(new DefaultComboBoxModel(ImageFilters.values()));

            int minP = 0, maxP = 100;
            @SuppressWarnings("UseOfObsoleteCollectionType") Dictionary<Integer, JComponent> powerDictionary = new Hashtable<>();
            powerDictionary.put(minP, new JLabel(String.valueOf(minP)));
            powerDictionary.put(maxP, new JLabel(String.valueOf(maxP)));
            PowerSlider = new JSlider();
            PowerSlider.setLabelTable(powerDictionary);
            UIUtil.setSliderIsFilled(PowerSlider, Boolean.TRUE);
            PowerSlider.setMinimum(minP);
            PowerSlider.setMaximum(maxP);
            PowerSlider.setPaintLabels(true);
            PowerSlider.setPaintTicks(true);
            PowerSlider.setPaintTrack(true);
            PowerSlider.setMajorTickSpacing(maxP);
            PowerSlider.setMinorTickSpacing(10);
            PowerSlider.setSnapToTicks(true);


            JComboBox<String> comboBoxSource = new ComboBox<>(new CollectionComboBoxModel<>(), 1000);
            JComboBox<String> comboBoxResult = new ComboBox<>(new CollectionComboBoxModel<>(), 1000);

            sourcePath = new ComboboxWithBrowseButton(comboBoxSource);
            resultPath = new ComboboxWithBrowseButton(comboBoxResult);

            sourcePath.getComboBox().setEditable(true);
            resultPath.getComboBox().setEditable(true);
            FileChooserDescriptor descriptorSource =
                    new FileChooserDescriptor(true, false, false, false, false, false)
                            .withFileFilter(file -> ImageFileTypeManager.getInstance().isImage(file));
            FileChooserDescriptor descriptorResult =
                    new FileChooserDescriptor(false, true, false, false, false, false)
                            .withFileFilter(VirtualFile::isDirectory);
            sourcePath.addBrowseFolderListener(null, null, null, descriptorSource, TextComponentAccessor.STRING_COMBOBOX_WHOLE_TEXT);
            resultPath.addBrowseFolderListener(null, null, null, descriptorResult, TextComponentAccessor.STRING_COMBOBOX_WHOLE_TEXT);
            restoreRecentImages();
        }

        private void storeRecentImages() {
            List<String> items = getComboModel().getItems();
            AppSettingsState.getInstance().storedSourcePaths =
                    StringUtil.join(ContainerUtil.getFirstItems(items, 5), "\n");
        }

        private void restoreRecentImages() {
            String paths = AppSettingsState.getInstance().storedSourcePaths;
            CollectionComboBoxModel<String> model = getComboModel();
            if (paths == null) return;
            for (String s :
                    paths.split("\n")) {
                if (!StringUtil.isEmptyOrSpaces(s) && !model.contains(s)) {
                    model.add(s);
                }
            }
        }

        private CollectionComboBoxModel<String> getComboModel() {
            //noinspection unchecked
            return ((CollectionComboBoxModel<String>) sourcePath.getComboBox().getModel());
        }
    }
}
