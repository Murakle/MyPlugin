package Settings;

import Filter.ImageFilters;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.util.xmlb.XmlSerializerUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;
import java.util.List;

// Support of storing the application settings in a persistent way;
@State(
        name = "Settings.AppSettingsState",
        storages = {@Storage("MyPluginSettings.xml")}
)
public class AppSettingsState implements PersistentStateComponent<AppSettingsState> {
    public int index = 0;
    public String src = "";
    public String res = "";

    public String storedSourcePaths = "";
    public ImageFilters filter = ImageFilters.NONE;
    public int power = 0;

    public static AppSettingsState getInstance() {
        return ServiceManager.getService(AppSettingsState.class);
    }

    @Nullable
    @Override
    public AppSettingsState getState() {
        return this;
    }

    @Override
    public void loadState(@NotNull AppSettingsState state) {
        XmlSerializerUtil.copyBean(state, this);
    }
}
