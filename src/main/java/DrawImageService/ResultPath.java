package DrawImageService;

import Settings.AppSettingsState;
import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.wm.impl.IdeBackgroundUtil;

import java.io.File;

public class ResultPath {
    private final static String fileNamePart1 = "MyPluginResult";
    private final static String fileNamePart2 = ".jpg";

    public static String GetPath() {
        AppSettingsState mySettings = AppSettingsState.getInstance();
        int index = mySettings.index;
        return mySettings.res + fileNamePart1 + index + fileNamePart2;
    }

    public static void RemoveOldPath() {
        AppSettingsState mySettings = AppSettingsState.getInstance();
        int index = mySettings.index - 1;
        File oldFile = new File(mySettings.res + fileNamePart1 + index + fileNamePart2);
        oldFile.delete();

    }
}
