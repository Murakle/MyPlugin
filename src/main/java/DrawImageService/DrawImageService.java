package DrawImageService;

import Filter.ImageFilters;
import Settings.AppSettingsState;
import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.wm.impl.IdeBackgroundUtil;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ScheduledExecutorService;

public class DrawImageService {


    private static String resultPath;
    private static String sourcePath;
    private static ImageFilters filter;
    private static int power;
    private static BufferedImage source;

    public static void Draw() throws IOException {
        AppSettingsState mySettings = AppSettingsState.getInstance();
        sourcePath = mySettings.src;
        resultPath = mySettings.res;
        filter = mySettings.filter;
        power = mySettings.power;

        try {
            source = ImageIO.read(new File(sourcePath));
        } catch (IOException e) {
            throw new RuntimeException("Not correct input path");
        }
        run();
    }

    public static void run() throws IOException {
        BufferedImage result = filter.getFilter(source, power).apply().getResult();
        File dir = new File(resultPath);
        if (!dir.exists() || !dir.isDirectory()) {
            throw new IOException("Not correct output path: " + resultPath);
        }
        String resultImagePath = ResultPath.GetPath();
        System.out.println(resultImagePath);
        ResultPath.RemoveOldPath();
        ImageIO.write(result, "jpg", new File(resultImagePath));
        PropertiesComponent properties = PropertiesComponent.getInstance();
        properties.setValue(IdeBackgroundUtil.EDITOR_PROP, resultImagePath);
        IdeBackgroundUtil.repaintAllWindows();
    }

}
