package Filter;

import java.awt.image.BufferedImage;

public interface IFilter {
    FilterResult apply();
    IFilter setParameters(BufferedImage image, int power);
    @SuppressWarnings("UndesirableClassUsage")
    default BufferedImage copyImage(BufferedImage image) {
        return new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
    }
}
