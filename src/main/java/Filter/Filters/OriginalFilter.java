package Filter.Filters;


import Filter.FilterResult;
import Filter.IFilter;

import java.awt.image.BufferedImage;

public class OriginalFilter implements IFilter {
    private BufferedImage image;

    public OriginalFilter() {
    }


    public OriginalFilter(BufferedImage image, int power) {
        this.image = image;
    }

    public OriginalFilter(BufferedImage image) {
        this(image, 0);
    }

    @Override
    public FilterResult apply() {
        return new FilterResult(image);
    }

    @Override
    public IFilter setParameters(BufferedImage image, int power) {
        this.image = image;
        return this;
    }
}
