package Filter.Filters;

import Filter.FilterResult;
import Filter.IFilter;

import java.awt.*;
import java.awt.image.BufferedImage;


@SuppressWarnings("UseJBColor")
public class LinearBlurFilter implements IFilter {
    private BufferedImage image;
    private int power;

    //power from 0 to 1 (0 - no blur, 1- max blur)
    public LinearBlurFilter() {
    }

    @Override
    public FilterResult apply() {
        BufferedImage result = copyImage(image);
        int boxSize = getBoxSize();
        float div = (float) 1 / (boxSize * boxSize);
        int[][][] resultImage = sumsOfSubRect(boxSize);
        for (int i = 0; i < result.getWidth(); i++) {
            for (int j = 0; j < result.getHeight(); j++) {
                int xx = i + boxSize - 1 + boxSize / 2;
                int yy = j + boxSize - 1 + boxSize / 2;
                int c[] = new int[3];
                for (int k = 0; k < 3; k++) {
                    c[k] = resultImage[xx][yy][k]
                            - resultImage[xx][yy - boxSize][k]
                            - resultImage[xx - boxSize][yy][k]
                            + resultImage[xx - boxSize][yy - boxSize][k];
                }

                result.setRGB(i, j, new Color(
                        normalize((int) (c[0] * div), 256),
                        normalize((int) (c[1] * div), 256),
                        normalize((int) (c[2] * div), 256)).getRGB());
            }
        }
        return new FilterResult(result);
    }

    @Override
    public IFilter setParameters(BufferedImage image, int power) {
        this.image = image;
        this.power = power;
        return this;
    }

    private int normalize(int number, int limitation) {
        return Math.min(limitation - 1, Math.max(number, 0));
    }

    private int getBoxSize() {
        int s = (power / 10);
        return s + (s + 1) % 2;
    }


    private int[][][] sumsOfSubRect(int boxSide) {
        int w = image.getWidth() + (boxSide - 1) * 2;
        int h = image.getHeight() + (boxSide - 1) * 2;
        int[][][] res = new int[w][h][3];
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                int xx = normalize(i - boxSide + 1, image.getWidth());
                int yy = normalize(j - boxSide + 1, image.getHeight());

                Color c = new Color(image.getRGB(xx, yy));

                for (int k = 0; k < 3; k++) {
                    res[i][j][k] = (i > 0 ? res[i - 1][j][k] : 0)
                            + (j > 0 ? res[i][j - 1][k] : 0)
                            - (i > 0 && j > 0 ? res[i - 1][j - 1][k] : 0);
                    res[i][j][k] += (c.getRGB() >> (16 - k * 8)) & 0xFF;
                }
            }
        }
        return res;
    }

}
