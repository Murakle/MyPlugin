package Filter;

import Filter.Filters.BlackWhiteFilter;
import Filter.Filters.LinearBlurFilter;
import Filter.Filters.OriginalFilter;

import java.awt.image.BufferedImage;


public enum ImageFilters {
    NONE(0, new OriginalFilter()),
    BLACK_WHITE(1, new BlackWhiteFilter()),
    LINEAR_BLUR_FILTER(2, new LinearBlurFilter()),
    ;

    int index;
    IFilter creator;

    ImageFilters(int index, IFilter creator) {
        this.creator = creator;
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public IFilter getFilter(BufferedImage image, int power) {
        return creator.setParameters(image, power);
    }
}
